import styles from "./User.module.css";

export default function User({ user, onLogout }) {
  return (
    <div className={styles.container}>
      <div className={styles.header}>
        <img src={user.image} height={100} width={100} alt="профиль" />
        <h1>Вы вошли как {user.username}</h1>
      </div>
      <div className={styles.userFields}>
        <div className={styles.userRow}>
          <label>Имя:</label>
          <span>{user.firstName}</span>
        </div>

        <div className={styles.userRow}>
          <label>Фамилия:</label>
          <span>{user.lastName}</span>
        </div>

        <div className={styles.userRow}>
          <label>Почта:</label>
          <span>{user.email}</span>
        </div>
      </div>
      <button className={styles.button} onClick={onLogout}>
        Выйти
      </button>
    </div>
  );
}
