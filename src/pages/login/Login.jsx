import { useState } from "react";
import axios from "axios";
import Loader from "@components/loader/Loader";
import styles from "./Login.module.css";

export default function Login({ onLogin }) {
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  function handleSubmit(event) {
    event.preventDefault();

    setLoading(true);
    setError("");

    const body = {
      username: name,
      password: password,
    };

    axios
      .post("/auth/login", body)
      .then((response) => {
        console.log(response);
        onLogin(response.data);
      })
      .catch((error) => {
        console.log(error);
        if (error.response.status === 400) {
          setError(
            "Не удалось войти в систему. Пользователь с такими логином и паролем не найден!"
          );
        } else {
          setError("Ошибка при выполнении запроса!");
        }
      })
      .finally(() => setLoading(false));
  }

  if (loading) return <Loader />;

  return (
    <div className={styles.container}>
      <h1 className={styles.header}>Войти</h1>
      <form onSubmit={handleSubmit}>
        <div className={styles.textField}>
          <label htmlFor="userLogin">Логин</label>
          <input
            id="userLogin"
            type="text"
            className={styles.textInput}
            placeholder="введите логин"
            value={name}
            onChange={(event) => setName(event.target.value)}
            required
          />
        </div>

        <div className={styles.textField}>
          <label htmlFor="userPassword">Пароль</label>
          <input
            id="userPassword"
            type="text"
            className={styles.textInput}
            placeholder="введите пароль"
            value={password}
            onChange={(event) => setPassword(event.target.value)}
            required
          />
        </div>

        {error && <p style={{ color: "red" }}>{error}</p>}

        <button className={styles.button} type="sumbit">
          Отправить
        </button>
      </form>
    </div>
  );
}
