import { useState } from 'react';
import styles from './App.module.css';
import Login from './pages/login/Login';
import User from './pages/user/User';

function App() {
  const [user, setUser] = useState(null)

  return (
    <div className={styles.content}>
      {user 
        ? <User user={user} onLogout={() => setUser(null)}/> 
        : <Login onLogin={(user) => setUser(user)}/>}
    </div>   
  );
}

export default App;
